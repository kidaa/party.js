Final Fantasy Trigger Guide
===========================

0. Start game
    - Opens Temple of Fiends (1)
1. Defeat Garland
    - Opens Matoya's Cave
2. Defeat Pirates
    - Opens Dwarven Cave (1)
    - Opens Marsh Cave (1)
3. Fetch crown
4. Defeat Astos
5. Trade Crystal Eye for Jolt Tonic
6. Awake Prince
    - Opens Elven Castle
    - Opens Astos Castle
    - Opens Marsh Cave (2)
    - Opens Cornelia Castle
    - Opens Temple of Fiends (2)
    - Opens Dwarven Cave (2)
7. Obtain Nitro Powder
8. Create canal
    - Opens Melmond (shops)
    - Opens Crescent Lake (shops)
9. Defeat Vampire
10. Obtain Earth Rod
11. Defeat Lich/Obtain Earth Crystal
12. Obtain Canoe
    - Opens Mount Gulug
    - Opens Ice Cave
    - Opens Castle of Ordeals
13. Defeat Kary/Obtain Fire Crystal
13. Obtain Float Stone
14. Obtain Airship
    - Opens Class Change
    - Opens Gaia
15. Obtain Bottled Fairy
16. Obtain Oxyale
    - Opens Rosetta Stone

Future Shopping Guide
---------------------
