Custom character class ideas
============================

Mechanics
---------

- High white/high black magic user
    - offset with double MP cost?
    - ... insanely low offense/defense?
        - complete lack of fight command/weaponry?
- Cover low HP characters
    - taunt (change tanking priority)
- Upgrade resistance to absorbtion
- Dual action
    - implemented as an additional, asynchronous/unpaired action in the battle action queue
    - Any combination of fight/item/magic
- Monster tamer/copier/summoner (esper style, high cost, once per battle)
    - One of each family?
        - Spirit
        - Dragon
        - Giant
        - Undead
        - Were
        - Aquatic
        - Mage
        - Regenerative
    - mimic defeated monsters
        - using exact script
- Swordsman
    - Binds element or status to attack, cost MP
- Free random spell cast
    - Offset low spells more common
    - Completely random target/family
    - Or focus white, focus black
- Sacrifice HP
    - Multi target attack (dark wave)
- Amplify items (Chemist)
- Escape (Bard)
    - Hide low HP self or low HP allies
- Spend MP
    - Cause crit/boost attack
- Vampirism
    - Syphon HP/MP
        - Offset by low defense (continually low HP)
- Reflection
    - Cost offset by complexity of or damage caused by enemy spell
- Jumping
    - Two slot action, evade + attack
- Stealing
    - Grant gold/experience/item
- Channel damage/heal over time
    - Or maybe magic lantern style decay, 1/1, 1/2, 1/4, 1/8, 1/16, reset
- Auto pilot
    - Uncontrollable character, AI
    - Best available action (attack, drink, item, spell??) given immediate queue position
        - Versus queueing blind and acting whenever
- Necromancy (death = bonus)
    - Enemy deaths = bonus?
    - Ally death = strength bonus (Death Sickle)
- Reversi mode
    - play as monster party
    - menu has skills
    - AI scripts for player class "monster" types

Generation
----------

Spells. Pick:

- element
- status
- effectiveness
- target
- function

Enemies. Pick:

- portrait
- family
- status affinity
- elemental affinity
- maybe code element => color, status => portrait for semantic linking

Bit notes
=========

- 8: 256, 128, 64, 32, 16, 8, 4, 2, 1
- 4: 16, 8, 4, 2, 1
- 2: 4, 2, 1
- 1: 2, 1
