party.js
========

Final Fantasy is a popular role-playing video game for the Nintendo Entertainment System. One feature in particular gave it a lot of charm and replay value, namely the abiity to create parties (combinations of character classes). Each party had various pros and cons. I figured, why not use a computer to explore the various combinations and using a set of rules, come up with automated descriptions about how each party performs.

Features
--------

- generate all combinations
- note special parties
    - nes default
    - modern default
    - alt party
    - black belt magic
    - magic users
    - heavy magic users
    - no magic
    - single class challenge
    - box party (alt party + black mage)
- badges
    - has knight / excalibur
    - has ninja / katana
    - has grand master / bare fist
    - has red wizard / four crystals
    - can white / cure
    - has white wizard / life2
    - can black / fire
    - has black wizard / nuke
    - solo badge / 1
    - death badge / reaper
    - single class challenge / warrior
- filter
    - has+
    - physical/magical balance more less even
    - is diverse (one of each)
    - number of characters
    - show upgraded
    - can magic
        - 0: cannot
        - 1: class change
        - 2: native red
        - 3: native white/black
- support for death slots
    - solo
- "easy mode", has fighter in first slot
- armor/magic cost per character
- magic abilities
    - has
    - highest
- physical fight power
- party signature (1234560)
- dynamic notes
    - gold cost
    - offensive tactics (single target vs multi target)
    - black tactics (single wizard vs effects)
    - white tactics (single healer vs harming)
    - healing (dedicated healer vs fighters; white mage or duplicates)
