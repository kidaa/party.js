var WarpCube = { };

WarpCube.FIGHTER    = 0x1;
WarpCube.THIEF      = 0x2;
WarpCube.BLACK_BELT = 0x3;
WarpCube.RED_MAGE   = 0x4;
WarpCube.WHITE_MAGE = 0x5;
WarpCube.BLACK_MAGE = 0x6;

WarpCube.Warriors = [
    {
        name: 'Fighter',
        promotion: 'Knight',
        name_abbr: 'Fi',
        promo_abbr: 'Kn',
        physical: 1,
        magical: 0,
        can_magic: 2,
        can_white: 1,
        can_black: 0,
        cost: 3,
        value: WarpCube.FIGHTER
    },
    {
        name: 'Thief',
        promotion: 'Ninja',
        name_abbr: 'Th',
        promo_abbr: 'Ni',
        physical: 1,
        magical: 0,
        can_magic: 2,
        can_white: 0,
        can_black: 1,
        cost: 1,
        value: WarpCube.THIEF
    },
    {
        name: 'Black Belt',
        promotion: 'Grand Master',
        name_abbr: 'BB',
        promo_abbr: 'GM',
        physical: 1,
        magical: 0,
        can_magic: 0,
        can_white: 0,
        can_black: 0,
        cost: 0,
        value: WarpCube.BLACK_BELT
    },
    {
        name: 'Red Mage',
        promotion: 'Red Wizard',
        name_abbr: 'RM',
        promo_abbr: 'RW',
        physical: 1,
        magical: 1,
        can_magic: 1,
        can_white: 1,
        can_black: 1,
        cost: 3,
        value: WarpCube.RED_MAGE
    },
    {
        name: 'White Mage',
        promotion: 'White Wizard',
        name_abbr: 'WM',
        promo_abbr: 'WW',
        physical: 0,
        magical: 1,
        can_magic: 1,
        can_white: 1,
        can_black: 0,
        cost: 4,
        value: WarpCube.WHITE_MAGE
    },
    {
        name: 'Black Mage',
        promotion: 'Black Wizard',
        name_abbr: 'BM',
        promo_abbr: 'BW',
        physical: 0,
        magical: 1,
        can_white: 1,
        can_black: 0,
        cost: 4,
        value: WarpCube.BLACK_MAGE
    }
];

WarpCube._Party = function (constArray) {
    this.warriors = constArray;

    var updateState = function (that) {
        that.length = that.warriors.length;
        that.isSolo = that.warriors.length == 1;
        that.hasDeath = that.warriors.length < 4;
        that.physical = 0;

        var firstClass = that.warriors[0];
        that.isSingleClass = true;

        for (var i = 1; i < that.warriors.length; i++) {
            that.isSingleClass = firstClass == that.warriors[i];
        }

        that.signature = '';

        for (var i = 0; i < that.warriors.length; i++) {
            that.signature += that.warriors[i];
        }
    }

    this.unshift = function (warriorCode) {
        this.warriors.unshift(warriorCode);
        updateState(this);
    }

    updateState(this);
}

WarpCube.Advisor = function () {
    this.parties = WarpCube.recurseParties(1, 4, 1)

    for (var i = 0; i < this.parties.length; i++) {
        var party = this.parties[i];

        console.log( party.signature );
        console.log( party );

        if (party.isSolo) {
            console.log(' - this is a solo challenge!');
        } else if (party.hasDeath) {
            console.log(' - Consider increasing the difficulty of the game by intentionally killing off one or more party members. The remaining characters will level up more quickly. But they will also be vulnerable to more attacks.');
        }

        if (party.isSingleClass) {
            console.log(' - this is a single class challenge!');
        }

        if (party.signature == '1356') {
            console.log(' - classic balanced party!');
        }

        if (party.signature == '1234') {
            console.log(' - default party!');
        }

        if (party.signature == '3356') {
            console.log(' - veteran party!');
        }

        if (party.signature == '1456') {
            console.log(' - magic party!');
        }

        if (party.signature == '4566') {
            console.log(' - heavy magic party!');
        }

        if (party.signature == '1233') {
            console.log(' - powerful fighting!');
        }

        if (party.signature == '1236') {
            console.log(' - box party!');
        }

        if (party.physical < 2) {
            console.log(' - This party lacks physical strength. A lot of time will be spent grinding!');
        }
    }

    console.log(this.parties.length);
};

WarpCube.recurseParties = function (minimumWarrior, maximumDepth, currentDepth) {
    var parties = [];

    for (var i = minimumWarrior; i <= 6; i++) {
        parties.push(new WarpCube._Party([ i ]));

        if (maximumDepth == currentDepth) {
            continue;
        }

        var subParties = WarpCube.recurseParties(i, maximumDepth, currentDepth + 1);

        for (var j = 0; j < subParties.length; j++) {
            var currentSubParty = subParties[j];
            currentSubParty.unshift(i);

            parties.push( currentSubParty );
        }
    }

    return parties;
}
