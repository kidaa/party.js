new WarpCube.Advisor();

WarpCube.Controller = function () { }

WarpCube.Controller.updateHandler = function () {
    var html = '';

    $('#parties').html(Math.random() + html);
}

WarpCube.Controller.prototype.appendCheckbox = function (containerId, name, value, id, labelText) {
    var input = $('<input>');

    input.attr('type', 'checkbox');
    input.attr('name', name);
    input.attr('value', value);
    input.attr('id', id);

    input.change(WarpCube.Controller.updateHandler);

    var div = $('<div>');
    div.append(input);

    var label = $('<label>');
    label.attr('for', id);
    label.append(labelText);
    label.css('padding-left', '.3em');

    div.append(label);

    $('#' + containerId).append(div);
}

$(function () {
    var controller = new WarpCube.Controller;

    $.each(WarpCube.Warriors, function (i, v) {
        controller.appendCheckbox(
            'classes',
            'class',
            v.value,
            'c' + v.value,
            v.name + ' / ' + v.promotion
        );

        var n = i + 1;

        var option = $('<option>');
        option.attr('value', n);
        option.append(n);

        if (n == 4)
            option.attr('selected', 'selected');

        $('select').append(option);
    });

    controller.appendCheckbox(
        'options',
        'canonical',
        1,
        'canonical',
        'Limit to canonical parties'
    );

    controller.appendCheckbox(
        'options',
        'death',
        1,
        'death',
        'Include smaller parties'
    );

    $('select').change(WarpCube.Controller.updateHandler);
    WarpCube.Controller.updateHandler();
});
